//
//  Inputs.h
//  hybrid_1d
//
//  Created by KYUNGGUK MIN on 1/14/19.
//  Copyright © 2019 Kyungguk Min & Kaijun Liu. All rights reserved.
//

#ifndef Inputs_h
#define Inputs_h

/// simulation input parameters;
/// modify the variables under the `Input' namespace
/// consult "Predefined.h" header for symbol definitions and constants
///
struct Input {
    //
    // MARK:- Housekeeping
    //

    /// number of subdomains for domain decomposition (positive integer)
    ///
    /// Nx must be divisible by this number
    ///
    static constexpr unsigned number_of_subdomains = 40;

    /// number of worker threads to spawn for parallelization
    ///
    /// value `0' means serial update; value `n' means parallelization using n + 1 threads
    /// part_desc.Nc*Nx must be divisible by n + 1, and
    /// n + 1 must be divisible by number_of_subdomains
    ///
    static constexpr unsigned number_of_worker_threads = number_of_subdomains - 1;

    /// electric field extrapolation method
    ///
    static constexpr Algorithm algorithm = PC;

    /// number of subscyles for magnetic field update; applied only for CAM-CL algorithm
    ///
    static constexpr unsigned n_subcycles = 10;

    //
    // MARK: Global parameters
    //

    /// light speed
    ///
    static constexpr Real c = 135.499;

    /// magnitude of uniform background magnetic field
    ///
    static constexpr Real O0 = 1;

    /// angle in degrees between the x-axis and the uniform magnetic field direction.
    ///
    static constexpr Real theta = 0;

    /// simulation grid size
    ///
    static constexpr Real Dx = 0.4;

    /// number of grid points
    ///
    static constexpr unsigned Nx = 1440;

    /// time step size
    ///
    static constexpr Real dt = 0.02;

    /// number of time steps for inner loop
    /// total time step Nt = inner_Nt * outer_Nt
    /// simulation time t = dt*Nt
    ///
    static constexpr unsigned inner_Nt = 5;

    /// number of time steps for outer loop
    /// total time step Nt = inner_Nt * outer_Nt
    /// simulation time t = dt*Nt
    ///
    static constexpr unsigned outer_Nt = 20000;

    //
    // MARK: Plasma Species Descriptions
    //

    /// charge-neutralizing electron fluid description
    ///
    static constexpr auto efluid_desc = eFluidDesc({-1836, 5805.94}, 0.0001, adiabatic);

    /// kinetic plasma descriptors
    ///
    static constexpr unsigned Nc = 66666;
    static constexpr auto part_descs =
    std::make_tuple(// hot p
                    BiMaxPlasmaDesc({{1, 60.597, 2}, Nc, _2nd, full_f}, 0.195679, 2.5),
                    // hot He
                    BiMaxPlasmaDesc({{.25, 30.2985, 2}, Nc, _2nd, full_f}, 0.195679, 2.5),
                    // cold p
                    BiMaxPlasmaDesc({{1, 95.8123, 1}, Nc/10, _2nd, full_f}, 0.00005),
                    // cold He
                    BiMaxPlasmaDesc({{.25, 21.4243, 1}, Nc/10, _2nd, full_f}, 0.00001)
                    );

    /// cold fluid plasma descriptors
    ///
    static constexpr auto cold_descs =
    std::make_tuple();

    //
    // MARK: Data Recording
    //

    /// a top-level directory to which outputs will be saved
    ///
    static constexpr char working_directory[] = "./data";

    /// field and particle energy density recording frequency; in units of inner_Nt
    /// `0' means `not interested'
    ///
    static constexpr unsigned energy_recording_frequency = 2;

    /// electric and magnetic field recording frequency
    ///
    static constexpr unsigned field_recording_frequency = 10;

    /// ion species moment recording frequency
    ///
    static constexpr unsigned moment_recording_frequency = 20;

    /// simulation particle recording frequency
    ///
    static constexpr unsigned particle_recording_frequency = 1000;

    /// maximum number of particles to dump
    ///
    static constexpr std::array<unsigned,
    std::tuple_size_v<decltype(part_descs)>> Ndumps = {0, 0, 1000000, 1000000};

    /// velocity histogram recording frequency
    ///
    static constexpr unsigned vhistogram_recording_frequency = 200;

    /// per-species gyro-averaged velocity space specification used for sampling velocity histogram
    ///
    /// the parallel (v1) and perpendicular (v2) velocity specs are described by
    /// the range of the velocity space extent and the number of velocity bins
    ///
    /// note that the Range type is initialized with the OFFSET (or location) and the LENGTH
    ///
    /// recording histograms corresponding to specifications with the bin count being 0 will be skipped over
    ///
    static constexpr std::array<std::pair<Range, unsigned>,
    std::tuple_size_v<decltype(part_descs)>> v1hist_specs = {
        std::make_pair(Range{-4, 8}/1.0, 200), // hot p
        std::make_pair(Range{-4, 8}/2.0, 200), // hot He
        std::make_pair(Range{-1, 2}/10., 200), // cool p
        std::make_pair(Range{-1, 2}/10., 200)  // cool He
    };
    static constexpr std::array<std::pair<Range, unsigned>,
    std::tuple_size_v<decltype(part_descs)>> v2hist_specs = {
        std::make_pair(Range{0, 5}/1.0, 110), // hot p
        std::make_pair(Range{0, 5}/2.0, 110), // hot He
        std::make_pair(Range{0, 2}/10., 100), // cool p
        std::make_pair(Range{0, 2}/10., 100)  // cool He
    };
};

/// debugging options
///
namespace Debug {
    constexpr bool zero_out_electromagnetic_field = false;
    constexpr Real initial_bfield_noise_amplitude = 0e0;
}

#endif /* Inputs_h */
