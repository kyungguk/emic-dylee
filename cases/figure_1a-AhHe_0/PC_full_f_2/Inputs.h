//
//  Inputs.h
//  hybrid_1d
//
//  Created by KYUNGGUK MIN on 1/14/19.
//  Copyright © 2019 Kyungguk Min & Kaijun Liu. All rights reserved.
//

#ifndef Inputs_h
#define Inputs_h

/// simulation input parameters;
/// modify the variables under the `Input' namespace
/// consult "Predefined.h" header for symbol definitions and constants
///
struct Input {
    //
    // MARK:- Housekeeping
    //

    /// number of worker threads to spawn for parallelization
    ///
    /// value `0' means serial update; value `n' means parallelization using n + 1 threads
    /// part_desc.Nc*Nx must be divisible by n + 1, and
    /// n + 1 must be divisible by number_of_subdomains
    ///
    static constexpr unsigned number_of_worker_threads = 39;

    /// number of subdomains for domain decomposition (positive integer)
    ///
    /// Nx must be divisible by this number
    ///
    static constexpr unsigned number_of_subdomains = number_of_worker_threads + 1;

    /// electric field extrapolation method
    ///
    static constexpr Algorithm algorithm = PC;

    /// number of subscyles for magnetic field update; applied only for CAM-CL algorithm
    ///
    static constexpr unsigned n_subcycles = 10;

    //
    // MARK: Global parameters
    //

    /// light speed
    ///
    static constexpr Real c = 135.499;

    /// magnitude of uniform background magnetic field
    ///
    static constexpr Real O0 = 1;

    /// angle in degrees between the x-axis and the uniform magnetic field direction.
    ///
    static constexpr Real theta = 0;

    /// simulation grid size
    ///
    static constexpr Real Dx = 0.4;

    /// number of grid points
    ///
    static constexpr unsigned Nx = 1440;

    /// time step size
    ///
    static constexpr Real dt = 0.02;

    /// number of time steps for inner loop
    /// total time step Nt = inner_Nt * outer_Nt
    /// simulation time t = dt*Nt
    ///
    static constexpr unsigned inner_Nt = 5;

    /// number of time steps for outer loop
    /// total time step Nt = inner_Nt * outer_Nt
    /// simulation time t = dt*Nt
    ///
    static constexpr unsigned outer_Nt = 20000;

    //
    // MARK: Plasma Species Descriptions
    //

    /// charge-neutralizing electron fluid description
    ///
    static constexpr auto efluid_desc = eFluidDesc({-1836, 5805.94}, 0.0001, adiabatic);

    /// kinetic plasma descriptors
    ///
    static constexpr unsigned Nc = 66666;
    static constexpr auto part_descs =
    std::make_tuple(// hot p (isotropic part)
                    BiMaxPlasmaDesc({{1, 38.3249, 1}, Nc/3, _2nd, full_f}, 0.0782715),
                    // hot p (high energy tail)
                    LossconePlasmaDesc({{1, 46.9383, 1}, Nc, _2nd, full_f}, 0.117407, 2.5, {0, 0.4}),
                    // hot He (isotropic part)
                    BiMaxPlasmaDesc({{.25, 30.2984, 1}, Nc/3, _2nd, full_f}, 0.195677),
                    // hot He (high energy tail)
                    LossconePlasmaDesc({{.25, 0.0958118, 1}, 10, _2nd, full_f}, 1.95677e-6, 1.00001, {0, 0.99999}),
                    // cold p
                    BiMaxPlasmaDesc({{1, 95.8123, 0}, Nc/100, _2nd, full_f}, 0.00005),
                    // cold He
                    BiMaxPlasmaDesc({{.25, 21.4243, 0}, Nc/100, _2nd, full_f}, 0.00001)
                    );

    /// cold fluid plasma descriptors
    ///
    static constexpr auto cold_descs =
    std::make_tuple(//ColdPlasmaDesc({1, 95.8123}), // cold p
                    //ColdPlasmaDesc({.25, 21.4243}) // cold He
                    );

    //
    // MARK: Data Recording
    //

    /// a top-level directory to which outputs will be saved
    ///
    static constexpr char working_directory[] = "./data";

    /// field and particle energy density recording frequency; in units of inner_Nt
    /// `0' means `not interested'
    ///
    static constexpr unsigned energy_recording_frequency = 2;

    /// electric and magnetic field recording frequency
    ///
    static constexpr unsigned field_recording_frequency = 10;

    /// ion species moment recording frequency
    ///
    static constexpr unsigned moment_recording_frequency = 20;

    /// simulation particle recording frequency
    ///
    static constexpr unsigned particle_recording_frequency = 1000;

    /// maximum number of particles to dump
    ///
    static constexpr std::array<unsigned,
    std::tuple_size_v<decltype(part_descs)>> Ndumps = {0, 0, 0, 0, 1000000, 1000000};

    /// velocity histogram recording frequency
    ///
    static constexpr unsigned vhistogram_recording_frequency = 200;

    /// per-species gyro-averaged velocity space specification used for sampling velocity histogram
    ///
    /// the parallel (v1) and perpendicular (v2) velocity specs are described by
    /// the range of the velocity space extent and the number of velocity bins
    ///
    /// note that the Range type is initialized with the OFFSET (or location) and the LENGTH
    ///
    /// recording histograms corresponding to specifications with the bin count being 0 will be skipped over
    ///
    static constexpr std::array<std::pair<Range, unsigned>,
    std::tuple_size_v<decltype(part_descs)>> v1hist_specs = {
        std::make_pair(Range{-4, 8}/1.0, 200), std::make_pair(Range{-4, 8}/1.0, 200),
        std::make_pair(Range{-4, 8}/2.0, 200), std::make_pair(Range{-4, 8}/2.0, 200),
        std::make_pair(Range{-3, 6}/10., 200), std::make_pair(Range{-3, 6}/10., 200)
    };
    static constexpr std::array<std::pair<Range, unsigned>,
    std::tuple_size_v<decltype(part_descs)>> v2hist_specs = {
        std::make_pair(Range{0, 4}/1.0, 100), std::make_pair(Range{0, 5}/1.0, 110),
        std::make_pair(Range{0, 4}/2.0, 100), std::make_pair(Range{0, 5}/2.0, 110),
        std::make_pair(Range{0, 3}/10., 100), std::make_pair(Range{0, 3}/10., 100)
    };
};

/// debugging options
///
namespace Debug {
    constexpr bool zero_out_electromagnetic_field = false;
    constexpr Real initial_bfield_noise_amplitude = 0e0;
}

#endif /* Inputs_h */
