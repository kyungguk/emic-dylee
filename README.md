# 1D Hybrid Code for Kinetic Simulations of the EMIC Instability Analyzed by [Lee et al. (2017)](https://doi.org/10.1002/2016JA023826)


## How to setup a simulation

Under the `cases` folder are five example setup configurations corresponding to the cases shown in Figure 1 of *Lee et al.*
Either modify `Inputs.h` directly in one of these directories, or duplicate one of the example cases and modify the input file.


## How to compile the code

In `Makefile` make necessary changes for your build environment and issue the `make all` command.
It should produce an executable named `hybrid_1d` in the build directory.

**GCC** or GCC-compatible compilers with the support for C++17 or above is required.


## How to run a simulation

The executable accepts the following command line options:
* `--wd=path/to/dir` overrides the `working_directory` option in the input file -- this sets the directory to which all output files will be dumped;
* `--outer_Nt={positive-integer}` overrides the outer loop count in the input file;
* `-save` instructs the code to take a snapshot of the latest state before termination;
* `-load` instructs the code to run from the saved state.

A good practice to run a time-consuming simulation is by running it incrementally.
The example below shows that after the first initializing run with `outer_Nt=1` and with the `-save` switch on and `-load` switch off,
the simulation is run in an increment of 100 steps with both `-save` and `-load` switches on.

    $ ./hybrid --wd=path/to/dir --outer_Nt=1 -save # this is the initializing run
    [log messages...]
    $ ./hybrid --wd=path/to/dir --outer_Nt=100 -save -load # repeat this command as many as you want
    [log messages...]
    $ ...


## Terms of Use

See the LICENSE file.
